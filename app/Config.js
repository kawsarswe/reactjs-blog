import axios from "axios";

export const Config = {
    apiUrl: 'http://127.0.0.1:8000/',
};

export default function () {
    axios.interceptors.request.use(function (config) {
        config.headers.Authorization = 'Bearer ' +localStorage.getItem('token');
        return config;
  });
}

module.exports = Config;