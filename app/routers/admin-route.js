import React from "react";
import axios from "axios";
import { Router, Route, IndexRoute, hashHistory } from "react-router";
import Login from "../components/Admin/Auth/Login";
import Dashboard from "../components/Admin/Dashboard/Dashboard";
import Config from "../Config";

function requireAuth(nextState, replace) {
    if (!localStorage.getItem('token')) {
        replace('login');
    }
}

const AdminRoutes = (
    <Route>
        <Route path='login' header='Admin Login' component={Login} />

        <Route path='/home' header='Dashboard' component={Dashboard} onEnter={requireAuth}>
            <IndexRoute header="Dashboard" component={Dashboard} />
        </Route>
    </Route>
);

export default AdminRoutes;