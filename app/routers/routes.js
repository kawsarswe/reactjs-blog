import React from "react";
import { Router, Route, IndexRoute, hashHistory } from "react-router";

import Main from '../components/Main';
import Home from '../components/Home';
import About from '../components/About/About';
import Contact from '../components/Contact/Contact';


import adminRoute from "../routers/admin-route"

var Config = require('../Config').appConfig;

var routes = (
    <Router history={hashHistory}>
        <Route path='/' config={Config} component={Main}>
            <IndexRoute header="Home" component={Home} />
            { adminRoute }
            <Route path='about' header='About' component={About} />
            <Route path='contact' header="Contact" component={Contact} />
        </Route>
    </Router>
)

module.exports = routes