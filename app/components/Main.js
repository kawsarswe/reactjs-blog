import React from 'react';
import Nav from "../components/Nav/Nav";
import Footer from "../components/Nav/Footer";

class Main extends React.Component {

    constructor(props) {
        super(props);
        this.config = props.route.config;
        /*this.state = {
         config: {}
         };*/
    }

    componentDidMount() {
    }

    render() {
        const { location } = this.props;
        return(
            <div>
                <Nav location={location} />
                {this.props.children}


                <Footer/>
            </div>
        )
    }
}

export default Main;