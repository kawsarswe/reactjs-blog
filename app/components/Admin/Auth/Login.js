import React from "react";
import axios from "axios";
import { Router, Redirect } from "react-router";
import Config from "../../../Config";


class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: props.route.header,
            data: [],
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        var _this = this;
        var url = Config.apiUrl + 'api/authenticate';
        axios.post(url, {
            email: this.state.email,
            password: this.state.password
        })
        .then(function (response) {
            localStorage.setItem('token', response.data.token);
            _this.props.history.push("/home");
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    componentDidMount(){
        document.title = this.state.title;
    }

    componentWillMount() {
    }

    render() {
        return (
            <div>
                <div className="">
                    <div className="login-box">
                        <div classNames="login-logo">
                            <a href="#"><b>My</b >Blog</a>
                        </div>

                        <div class="login-box-body">
                            <p class="login-box-msg">Sign in to start your session</p>

                            <form className="form-horizontal" onSubmit={this.handleSubmit}>
                                <div class="form-group has-feedback">
                                    <input type="email" name="email" class="form-control" placeholder="Email" onChange={this.handleInputChange} />
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" name="password" class="form-control" placeholder="Password" onChange={this.handleInputChange} />
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>

                                <div class="row">
                                    <div class="col-xs-4">
                                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

module.exports = Login;