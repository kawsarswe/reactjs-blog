import React from 'react';
import {Link, IndexLink} from "react-router";
class Nav extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        const { location } = this.props;
        const home = location.pathname === "/" ? "active" : "";
        const about = location.pathname.match(/^\/about|about/) ? "active" : "";
        const contact = location.pathname.match(/^\/contact|contact/) ? "active" : "";
        return(
            <div>
                <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
                    <div class="container-fluid">
                        <div class="navbar-header page-scroll">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                Menu <i class="fa fa-bars"></i>
                            </button>
                            <Link class="navbar-brand" to={'/'}>Kawsar</Link>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <IndexLink to={'/'} class={ home }>Home</IndexLink>
                                </li>
                                <li>
                                    <Link to={'about'} class={about}>About</Link>
                                </li>
                                <li>
                                    <Link to={'contact'} class={contact}>Contact</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        )
    }
}

export default Nav;