import React from 'react'
import ReactDom from 'react-dom'

import routes from './routers/routes'

ReactDom.render(routes, document.getElementById('app'));